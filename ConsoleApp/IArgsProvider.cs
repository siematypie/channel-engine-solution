﻿namespace ConsoleApp
{
    public interface IArgsProvider
    {
        public string[] Args { get; }
    }

    public class ArgsProvider : IArgsProvider
    {
        public ArgsProvider(string[] args)
        {
            Args = args;
        }

        public string[] Args { get; }
    }
}