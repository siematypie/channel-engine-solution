﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ardalis.SmartEnum;
using CommandDotNet;
using CommandDotNet.TypeDescriptors;

namespace ConsoleApp.TypeDescriptors
{
    public class SmartEnumNameTypeDescriptor<T> :
        IArgumentTypeDescriptor,
        IAllowedValuesTypeDescriptor
        where T : SmartEnum<T>
    {
        public IEnumerable<string> GetAllowedValues(IArgument argument) 
            => SmartEnum<T>.List.Select(e => e.Name);

        public bool CanSupport(Type type)
            => type.IsAssignableFrom(typeof(T));

        public string GetDisplayName(IArgument argument)
            => argument.TypeInfo.UnderlyingType.Name;

        public object ParseString(IArgument argument, string value) 
            => value is null ? null : SmartEnum<T>.FromName(value);
    }
}