﻿using System;

namespace ConsoleApp
{
    public class ConsoleOutputProvider : IOutputProvider
    {
        public void WriteLine(string output)
        {
            Console.WriteLine(output);
        }

        public void Write(string output)
        {
            Console.Write(output);
        }
    }

    public interface IOutputProvider
    {
        void WriteLine(string output);
        void Write(string output);
    }
}