﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Orders;
using Application.Products;
using CommandDotNet;
using ConsoleApp.Models;
using ConsoleApp.Views;
using Domain.Orders;
using MediatR;

namespace ConsoleApp
{
    public class AppOrchestrator
    {
        private readonly IMediator _mediator;
        private readonly IConsoleView<IEnumerable<OrderListItemDto>> _orderListDto;
        private readonly IOutputProvider _outputProvider;
        private readonly IConsoleView<ProductDetailsDto> _productDetailsView;
        private readonly IConsoleView<IEnumerable<TopSoldProductDto>> _topSoldProductsView;

        public AppOrchestrator(IMediator mediator,
            IConsoleView<ProductDetailsDto> productDetailsView,
            IConsoleView<IEnumerable<TopSoldProductDto>> topSoldProductsView,
            IConsoleView<IEnumerable<OrderListItemDto>> orderListDto,
            IOutputProvider outputProvider)
        {
            _mediator = mediator;
            _productDetailsView = productDetailsView;
            _topSoldProductsView = topSoldProductsView;
            _orderListDto = orderListDto;
            _outputProvider = outputProvider;
        }

        public async Task TopSoldProducts(
            [Option(LongName = "status",
                Description = "Order filter status, default no filter")]
            OrderStatus status = null,
            [Option(LongName = "quantity",
                Description = "Number of products to fetch, default Five")]
            AllowedQuantity quantity = AllowedQuantity.Five)
        {
            _outputProvider.WriteLine(
                $"\n---TOP {(int) quantity} SOLD IN ORDERS WITH {GetDisplayStatusName(status)}---");

            var products = await _mediator.Send(new ListTopProductsSold.Query
            {
                StatusFilter = status,
                QuantityFilter = (int) quantity
            });
            _topSoldProductsView.RenderView(products);
        }

        public async Task ProductDetails(
            [Option(LongName = "id",
                Description = "Merchant Product Number, required")]
            string merchantProductNo)
        {
            if (string.IsNullOrEmpty(merchantProductNo))
            {
                _outputProvider.WriteLine($"\n[VALIDATION ERROR] Merchant Product Number has to be provided for product details!");
                return;
            }

            _outputProvider.WriteLine($"\n---PRODUCT {merchantProductNo} DETAILS---");
            var product = await _mediator.Send(new GetProductDetails.Query {MerchantNo = merchantProductNo});
            _productDetailsView.RenderView(product);
        }

        public async Task SetProductStock(
            [Option(LongName = "id",
                Description = "Merchant Product Number, required")]
            string merchantProductNo,
            [Option(LongName = "value",
                Description = "Stock value, default 25")]
            int value = 25
        )
        {
            if (string.IsNullOrEmpty(merchantProductNo))
            {
                _outputProvider.WriteLine($"\n[VALIDATION ERROR] Merchant Product Number has to be provided for product stock update!");
                return;
            }
            if (value < 0)
            {
                _outputProvider.WriteLine($"\n[VALIDATION ERROR]: Product stock has to be non-negative number!");
                return;
            }
            _outputProvider.WriteLine($"\nUpdating Product {merchantProductNo} Stock to {value}");

            await _mediator.Send(new UpdateProductStock.Command
            {
                MerchantProductNo = merchantProductNo,
                Stock = value
            });

            _outputProvider.WriteLine("\nUpdate successful. Showing current details...");

            await ProductDetails(merchantProductNo);
        }

        public async Task ListOrders([Option(LongName = "status",
                Description = "Order filter status, default no filter")]
            OrderStatus status = null)
        {
            _outputProvider.WriteLine($"\n---ORDERS WITH {GetDisplayStatusName(status)} STATUS---");
            var orders = await _mediator.Send(
                new ListOrders.Query
                {
                    StatusFilter = status
                });
            _orderListDto.RenderView(orders);
        }

        private static string GetDisplayStatusName(OrderStatus status)
        {
            return status is null ? "ALL STATUSES" : $"{status.DisplayName.ToUpper()} STATUS";
        }
    }
}