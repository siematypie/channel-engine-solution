﻿namespace ConsoleApp.Models
{
    public enum AllowedQuantity
    {
        Five = 5,
        Ten = 10,
        Fifteen = 15
    }
}