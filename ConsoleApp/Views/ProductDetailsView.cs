﻿using Application.Products;
using ConsoleTables;

namespace ConsoleApp.Views
{
    public class ProductDetailsView : IConsoleView<ProductDetailsDto>
    {
        private readonly IOutputProvider _outputProvider;

        public ProductDetailsView(IOutputProvider outputProvider)
        {
            _outputProvider = outputProvider;
        }

        public void RenderView(ProductDetailsDto viewModel)
        {
            var table = new ConsoleTable("Property Name", "Value");
            table.AddRow("Ean", viewModel.Ean);
            table.AddRow("Merchant Product Number", viewModel.MerchantProductNo);
            table.AddRow("Price", viewModel.Price);
            table.AddRow("Size", viewModel.Size);
            table.AddRow("Brand", viewModel.Brand);
            table.AddRow("Color", viewModel.Color);
            table.AddRow("Stock", viewModel.Stock);

            _outputProvider.Write(table.ToStringAlternative());
            _outputProvider.WriteLine("\n");
        }
    }
}