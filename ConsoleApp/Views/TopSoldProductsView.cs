﻿using System.Collections.Generic;
using System.Linq;
using Application.Products;
using ConsoleTables;

namespace ConsoleApp.Views
{
    public class TopSoldProductsView : IConsoleView<IEnumerable<TopSoldProductDto>>
    {
        private readonly IOutputProvider _outputProvider;

        public TopSoldProductsView(IOutputProvider outputProvider)
        {
            _outputProvider = outputProvider;
        }

        public void RenderView(IEnumerable<TopSoldProductDto> viewModel)
        {
            var table = new ConsoleTable("Merchant Product Number", "Ean", "Name", "Quantity Sold");
            foreach (var item in viewModel)
                table.AddRow(item.MerchantProductNo, item.Ean, item.Name, item.QuantitySold);
            _outputProvider.Write(table.ToStringAlternative());
            if (!table.Rows.Any())
                _outputProvider.WriteLine("---Nothing to display---\n");
        }
    }
}