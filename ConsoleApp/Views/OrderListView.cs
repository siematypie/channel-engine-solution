﻿using System.Collections.Generic;
using System.Linq;
using Application.Orders;
using ConsoleTables;

namespace ConsoleApp.Views
{
    public class OrderListView : IConsoleView<IEnumerable<OrderListItemDto>>
    {
        private readonly IOutputProvider _outputProvider;

        public OrderListView(IOutputProvider outputProvider)
        {
            _outputProvider = outputProvider;
        }

        public void RenderView(IEnumerable<OrderListItemDto> viewModel)
        {
            var table = new ConsoleTable("Id", "Order Date", "Order Status", "Number of products", "Total", "Currency");
            foreach (var item in viewModel)
                table.AddRow(item.Id, item.OrderDate, item.OrderStatus.DisplayName, item.NumberOfProducts, item.Total,
                    item.Currency);
            _outputProvider.Write(table.ToStringAlternative());
            if (!table.Rows.Any())
                _outputProvider.WriteLine("---Nothing to display---\n");
        }
    }
}