﻿namespace ConsoleApp.Views
{
    public interface IConsoleView<in T>
    {
        void RenderView(T viewModel);
    }
}