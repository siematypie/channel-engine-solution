﻿using System.Collections.Generic;
using Application.Orders;
using Application.Products;
using ConsoleApp.Views;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SharedConfiguration;

namespace ConsoleApp
{
    internal class Program
    {
        private static void Main(params string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureLogging(SetUpLogging)
                .ConfigureAppConfiguration((context, config) => SetUpConfiguration(context, config, args))
                .ConfigureServices((context, serviceCollection) => SetUpServices(context, serviceCollection, args));
        }

        private static void SetUpServices(HostBuilderContext hostContext, IServiceCollection serviceCollection,
            string[] args)
        {
            serviceCollection.RegisterChannelEngineSharedDependencies(hostContext.Configuration)
                .AddSingleton<IOutputProvider, ConsoleOutputProvider>()
                .AddSingleton<IArgsProvider>(sp => new ArgsProvider(args))
                .AddSingleton<AppOrchestrator>()
                .AddSingleton<IConsoleView<ProductDetailsDto>, ProductDetailsView>()
                .AddSingleton<IConsoleView<IEnumerable<TopSoldProductDto>>, TopSoldProductsView>()
                .AddSingleton<IConsoleView<IEnumerable<OrderListItemDto>>, OrderListView>()
                .AddHostedService<Startup>();
        }

        private static void SetUpLogging(HostBuilderContext builder, ILoggingBuilder logging)
        {
            logging.ClearProviders();
            if (!builder.Configuration.GetValue<bool>("Logging:UseConsole"))
                logging.AddDebug();
            else
                logging.AddConsole();
        }

        private static void SetUpConfiguration(HostBuilderContext hostContext, IConfigurationBuilder config,
            string[] args)
        {
            var env = hostContext.HostingEnvironment;
            config.AddChannelEngineJsonSharedSettings(env)
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true)
                .AddEnvironmentVariables()
                .AddCommandLine(args);

            if (hostContext.HostingEnvironment.IsDevelopment())
                config.AddUserSecrets(typeof(ConfigurationRegistration).Assembly);
        }
    }
}