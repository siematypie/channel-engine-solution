﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Exceptions;
using CommandDotNet;
using CommandDotNet.IoC.MicrosoftDependencyInjection;
using ConsoleApp.TypeDescriptors;
using Domain.Orders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ConsoleApp
{
    public class Startup : IHostedService
    {
        private const string StandardErrorMessage =
            "Something went wrong... enable logging in configuration for more details";

        private readonly IArgsProvider _argsProvider;
        private readonly IHostApplicationLifetime _hostLifetime;
        private readonly ILogger<Startup> _logger;
        private readonly IOutputProvider _outputProvider;
        private readonly IServiceProvider _serviceProvider;

        public Startup(
            IHostApplicationLifetime hostLifetime,
            IOutputProvider outputProvider,
            IArgsProvider argsProvider,
            IServiceProvider serviceProvider,
            ILogger<Startup> logger)
        {
            _hostLifetime = hostLifetime;
            _outputProvider = outputProvider;
            _argsProvider = argsProvider;
            _serviceProvider = serviceProvider;
            _logger = logger;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Task.Factory.StartNew(DoWork, cancellationToken);
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _outputProvider.WriteLine("Thank you for using Channel Engine console solution");
            return Task.CompletedTask;
        }

        private async Task DoWork()
        {
            try
            {
                _hostLifetime.ApplicationStarted.WaitHandle.WaitOne(); //waits until host is started
                await new AppRunner<AppOrchestrator>()
                    .Configure(config =>
                    {
                        config.AppSettings.ArgumentTypeDescriptors.Add(
                            new SmartEnumNameTypeDescriptor<OrderStatus>());
                    })
                    .UseMicrosoftDependencyInjection(_serviceProvider)
                    .RunAsync(_argsProvider.Args);
            }
            catch (ChannelEngineAppException e)
            {
                var message = e.Status == HttpStatusCode.NotFound
                    ? "[ERROR: 404] Requested resource could not be found"
                    : $"[ERROR:{e.Status}] {StandardErrorMessage}";
                _outputProvider.WriteLine(message);
                _outputProvider.WriteLine("View/enable logs for more information");
                _logger.LogError(e, message);
            }
            catch (Exception e)
            {
                var message = $"[ERROR: 500] {StandardErrorMessage}";
                _outputProvider.WriteLine(message);
                _outputProvider.WriteLine("View/enable logs for more information");
                _logger.LogError(e, "Error occured");
            }
            finally
            {
                _hostLifetime.StopApplication();
            }
        }
    }
}