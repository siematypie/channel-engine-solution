﻿using System.Collections.Generic;

namespace ApiClient.Model
{
    public class ListResponse<T> : BasicResponse
    {
        public IList<T> Content { get; set; }
        public int Count { get; set; }
        public int TotalCount { get; set; }
        public int ItemsPerPage { get; set; }
    }
}