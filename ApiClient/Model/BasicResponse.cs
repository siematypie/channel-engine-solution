﻿namespace ApiClient.Model
{
    public class BasicResponse
    {
        public int StatusCode { get; set; }
        public bool Success { get; set; }
    }
}