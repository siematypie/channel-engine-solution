﻿namespace ApiClient.Model
{
    public class DetailedResponse<T> : BasicResponse
    {
        public T Content { get; set; }
    }
}