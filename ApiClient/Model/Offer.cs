﻿namespace ApiClient.Model
{
    public class Offer
    {
        public string MerchantProductNo { get; set; }
        public int Stock { get; set; }
    }
}