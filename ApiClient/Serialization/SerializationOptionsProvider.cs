﻿using System.Text.Json;
using ApiClient.Serialization.Converters;

namespace ApiClient.Serialization
{
    public class SerializationOptionsProvider : ISerializationOptionsProvider
    {
        public JsonSerializerOptions SerializerOptions
        {
            get
            {
                var serializeOptions = new JsonSerializerOptions();
                serializeOptions.Converters.Add(new OrderStatusConverter());
                serializeOptions.WriteIndented = true;
                return serializeOptions;
            }
        }
    }
}