﻿using System.Text.Json;

namespace ApiClient.Serialization
{
    public interface ISerializationOptionsProvider
    {
        JsonSerializerOptions SerializerOptions { get; }
    }
}