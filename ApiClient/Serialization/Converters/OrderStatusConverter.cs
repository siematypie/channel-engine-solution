﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using Domain.Orders;

namespace ApiClient.Serialization.Converters
{
    public class OrderStatusConverter : JsonConverter<OrderStatus>
    {
        public override OrderStatus Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            OrderStatus.TryFromName(reader.GetString(), out var status);
            return status;
        }

        public override void Write(Utf8JsonWriter writer, OrderStatus value, JsonSerializerOptions options)
        {
            writer.WriteStringValue(value.Name);
        }
    }
}