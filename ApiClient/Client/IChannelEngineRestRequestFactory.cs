﻿using RestSharp;

namespace ApiClient.Client
{
    public interface IChannelEngineRestRequestFactory
    {
        IRestRequest CreateRequest(string resourceSegment);
    }
}