﻿using ApiClient.Serialization;
using RestSharp;
using RestSharp.Serializers.SystemTextJson;

namespace ApiClient.Client
{
    public class ChannelEngineRestClientFactory : IChannelEngineRestClientFactory
    {
        private readonly string _apiUrl;
        private readonly ISerializationOptionsProvider _serializationOptionsProvider;

        public ChannelEngineRestClientFactory(IApiSettingsProvider settingsProvider,
            ISerializationOptionsProvider serializationOptionsProvider)
        {
            _serializationOptionsProvider = serializationOptionsProvider;
            _apiUrl = $"{settingsProvider.HostUrl}/{settingsProvider.Version}";
        }

        public IRestClient CreateChannelEngineClient()
        {
            return new RestClient(_apiUrl).UseSystemTextJson(_serializationOptionsProvider.SerializerOptions);
        }
    }
}