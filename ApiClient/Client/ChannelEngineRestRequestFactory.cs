﻿using RestSharp;

namespace ApiClient.Client
{
    public class ChannelEngineRestRequestFactory : IChannelEngineRestRequestFactory
    {
        private const string TokenParameterName = "apikey";
        private readonly string _token;

        public ChannelEngineRestRequestFactory(IApiSettingsProvider apiSettingsProvider)
        {
            _token = apiSettingsProvider.Token;
        }

        public IRestRequest CreateRequest(string resourceSegment)
        {
            return new RestRequest(resourceSegment).AddQueryParameter(TokenParameterName, _token);
        }
    }
}