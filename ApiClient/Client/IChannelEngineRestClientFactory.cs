﻿using RestSharp;

namespace ApiClient.Client
{
    public interface IChannelEngineRestClientFactory
    {
        IRestClient CreateChannelEngineClient();
    }
}