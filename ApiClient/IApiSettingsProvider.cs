﻿namespace ApiClient
{
    public interface IApiSettingsProvider
    {
        string HostUrl { get; }
        string Version { get; }
        string Token { get; }
        string ProductsEndpoint { get; }
        string OfferEndpoint { get; }
        string OrdersEndpoint { get; }
    }
}