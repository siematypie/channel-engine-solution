﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApiClient.Model;
using Application.InfrastructureContracts;
using Domain;

namespace ApiClient.Providers
{
    public class RestApiProductRepository : IProductsRepository
    {
        private readonly IOfferEndpointHandler _offerEndpointHandler;
        private readonly IProductEndpointHandler _productEndpointHandler;

        public RestApiProductRepository(IProductEndpointHandler productEndpointHandler,
            IOfferEndpointHandler offerEndpointHandler)
        {
            _productEndpointHandler = productEndpointHandler;
            _offerEndpointHandler = offerEndpointHandler;
        }

        public Task<IEnumerable<Product>> GetProductsByMerchantNumbers(IEnumerable<string> merchantNumbers)
        {
            return _productEndpointHandler.GetProductsByMerchantNumbers(merchantNumbers);
        }

        public Task<Product> GetProductByMerchantNumber(string merchantNumber)
        {
            return _productEndpointHandler.GetProductByMerchantNumber(merchantNumber);
        }

        public Task UpdateProductsStock(string merchantNumber, int newStock)
        {
            return _offerEndpointHandler.UpdateOffer(new Offer {MerchantProductNo = merchantNumber, Stock = newStock});
        }
    }
}