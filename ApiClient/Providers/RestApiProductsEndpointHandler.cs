﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApiClient.Client;
using ApiClient.Model;
using Domain;

namespace ApiClient.Providers
{
    public class RestApiProductsEndpointHandler : RestApiEndpointBase, IProductEndpointHandler
    {
        private const string MerchantNumberListQueryParamName = "merchantProductNoList";

        public RestApiProductsEndpointHandler(IApiSettingsProvider settingsProvider,
            IChannelEngineRestClientFactory clientFactory, IChannelEngineRestRequestFactory requestFactory)
            : base(clientFactory, requestFactory, settingsProvider.ProductsEndpoint)
        {
        }

        public async Task<IEnumerable<Product>> GetProductsByMerchantNumbers(IEnumerable<string> merchantNumbers)
        {
            var (client, request) = CreateRequestUtilities();
            foreach (var merchantNumber in merchantNumbers)
                request.AddQueryParameter(MerchantNumberListQueryParamName, merchantNumber);
            var result =
                await client.ExecuteGetAsync<ListResponse<Product>>(request);
            EnsureSuccess(result);
            return result.Data.Content;
        }

        public async Task<Product> GetProductByMerchantNumber(string merchantNumber)
        {
            var (client, request) = CreateRequestUtilities(merchantNumber);
            var result =
                await client.ExecuteGetAsync<DetailedResponse<Product>>(request);
            EnsureSuccess(result);
            return result.Data.Content;
        }
    }
}