﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApiClient.Client;
using ApiClient.Model;
using Application.InfrastructureContracts;
using Domain.Orders;

namespace ApiClient.Providers
{
    public class RestApiOrdersEndpointHandler : RestApiEndpointBase, IOrdersRepository
    {
        private const string StatusesParamName = "statuses";

        public RestApiOrdersEndpointHandler(IApiSettingsProvider settingsProvider,
            IChannelEngineRestClientFactory clientFactory, IChannelEngineRestRequestFactory requestFactory)
            : base(clientFactory, requestFactory, settingsProvider.OrdersEndpoint)
        {
        }

        public async Task<IEnumerable<Order>> GetAllOrders(OrderStatus statusFilter = null)
        {
            var (client, request) = CreateRequestUtilities();
            if (statusFilter != null)
                request.AddQueryParameter(StatusesParamName, statusFilter.Name);
            var result = await client.ExecuteGetAsync<ListResponse<Order>>(request);
            EnsureSuccess(result);
            return result.Data.Content;
        }
    }
}