﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain;

namespace ApiClient.Providers
{
    public interface IProductEndpointHandler
    {
        Task<IEnumerable<Product>> GetProductsByMerchantNumbers(IEnumerable<string> merchantNumbers);
        Task<Product> GetProductByMerchantNumber(string merchantNumber);
    }
}