﻿using System.Threading.Tasks;
using ApiClient.Model;

namespace ApiClient.Providers
{
    public interface IOfferEndpointHandler
    {
        Task UpdateOffer(Offer offer);
    }
}