﻿using System.Net;
using ApiClient.Client;
using Application.Exceptions;
using RestSharp;

namespace ApiClient.Providers
{
    public abstract class RestApiEndpointBase
    {
        protected readonly IChannelEngineRestClientFactory ClientFactory;
        protected readonly IChannelEngineRestRequestFactory RequestFactory;
        protected readonly string ResourceSegment;

        protected RestApiEndpointBase(IChannelEngineRestClientFactory clientFactory,
            IChannelEngineRestRequestFactory requestFactory, string resourceSegment)
        {
            ClientFactory = clientFactory;
            RequestFactory = requestFactory;
            ResourceSegment = resourceSegment;
        }

        protected (IRestClient client, IRestRequest request) CreateRequestUtilities(string additionalSegment = null)
        {
            return (ClientFactory.CreateChannelEngineClient(),
                RequestFactory.CreateRequest(GetResourceSegment(additionalSegment)));
        }

        private string GetResourceSegment(string additionalSegment)
        {
            return additionalSegment is null ? ResourceSegment : $"{ResourceSegment}/{additionalSegment}";
        }

        protected virtual void EnsureSuccess(IRestResponse response)
        {
            if (response.ResponseStatus != ResponseStatus.Completed)
                HandleNetworkTransportError(response);
            else if (response.ErrorException != null)
                HandleProcessingException(response);
            else if (!response.IsSuccessful)
                HandleUnsuccessfulResponse(response);
        }

        protected virtual void HandleProcessingException(IRestResponse response)
        {
            throw new ChannelEngineAppException(
                HttpStatusCode.InternalServerError,
                $"Failed to process request for resource {response.Request.Resource}. Check inner exception for details",
                new {ResponseContent = response.Content},
                response.ErrorException);
        }

        protected virtual void HandleUnsuccessfulResponse(IRestResponse response)
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.NotFound:
                    throw new ChannelEngineAppException(HttpStatusCode.NotFound,
                        $"Resource ${response.Request.Resource} could not be found");
                default:
                    throw new ChannelEngineAppException(HttpStatusCode.BadGateway,
                        "Channel Engine Api Service communication was unsuccessful",
                        new
                        {
                            ResponseContent = response.Content,
                            ExternalServiceStatusCode = response.StatusCode,
                            response.Request.Resource
                        });
            }
        }

        protected virtual void HandleNetworkTransportError(IRestResponse response)
        {
            throw new ChannelEngineAppException(HttpStatusCode.ServiceUnavailable,
                "Connection with Channel Engine Api failed",
                new {response.Request.Resource});
        }
    }
}