﻿using System.Threading.Tasks;
using ApiClient.Client;
using ApiClient.Model;
using RestSharp;

namespace ApiClient.Providers
{
    public class RestApiOfferEndpointHandler : RestApiEndpointBase, IOfferEndpointHandler
    {
        public RestApiOfferEndpointHandler(IChannelEngineRestClientFactory clientFactory,
            IChannelEngineRestRequestFactory requestFactory, IApiSettingsProvider settingsProvider) : base(
            clientFactory,
            requestFactory, settingsProvider.OfferEndpoint)
        {
        }

        public async Task UpdateOffer(Offer offer)
        {
            var (client, request) = CreateRequestUtilities();
            request.AddJsonBody(new[] {offer});
            request.Method = Method.PUT;
            var response = await client.ExecuteAsync(request);
            EnsureSuccess(response);
        }
    }
}