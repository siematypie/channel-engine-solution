﻿using System.IO;
using ApiClient;
using ApiClient.Client;
using ApiClient.Providers;
using ApiClient.Serialization;
using Application.InfrastructureContracts;
using Application.Orders;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace SharedConfiguration
{
    public static class ConfigurationRegistration
    {
        public static IServiceCollection RegisterChannelEngineSharedDependencies(
            this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMediatR(typeof(ListOrders).Assembly);
            var apiSettings = new ApiSettingsProvider();
            configuration.GetSection(ApiSettingsProvider.SettingsRootName).Bind(apiSettings);
            services.AddSingleton<IApiSettingsProvider, ApiSettingsProvider>(provider => apiSettings);
            services.AddSingleton<IChannelEngineRestRequestFactory, ChannelEngineRestRequestFactory>();
            services.AddSingleton<IChannelEngineRestClientFactory, ChannelEngineRestClientFactory>();
            services.AddSingleton<IOrdersRepository, RestApiOrdersEndpointHandler>();
            services.AddSingleton<IProductEndpointHandler, RestApiProductsEndpointHandler>();
            services.AddSingleton<IOfferEndpointHandler, RestApiOfferEndpointHandler>();
            services.AddSingleton<IProductsRepository, RestApiProductRepository>();
            services.AddSingleton<ISerializationOptionsProvider, SerializationOptionsProvider>();
            return services;
        }

        public static IConfigurationBuilder AddChannelEngineJsonSharedSettings
            (this IConfigurationBuilder builder, IHostEnvironment env)
        {
            var sharedFolder = Path.Combine(env.ContentRootPath, "..", "SharedConfiguration");
            return builder
                .AddJsonFile(Path.Combine(sharedFolder, "sharedSettings.json"), true,
                    true) // When running using dotnet run
                .AddJsonFile(Path.Combine(sharedFolder, $"sharedSettings.{env.EnvironmentName}.json"), true, true)
                .AddJsonFile("sharedSettings.json", true, true)
                .AddJsonFile($"sharedSettings.{env.EnvironmentName}.json", true, true);
        }
    }
}