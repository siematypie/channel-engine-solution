﻿using ApiClient;

namespace SharedConfiguration
{
    public class ApiSettingsProvider : IApiSettingsProvider
    {
        public const string SettingsRootName = "ApiSettings";
        public string HostUrl { get; set; }
        public string Version { get; set; }
        public string Token { get; set; }
        public string ProductsEndpoint { get; set; }
        public string OfferEndpoint { get; set; }
        public string OrdersEndpoint { get; set; }
    }
}