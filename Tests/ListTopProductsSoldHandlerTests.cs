using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.InfrastructureContracts;
using Application.Products;
using Domain;
using Domain.Orders;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace Tests
{
    public class ListTopProductsSoldHandlerTests
    {
        private Mock<IOrdersRepository> _ordersProviderMock;
        private Mock<IProductsRepository> _productsProviderMock;
        
        [SetUp]
        public void Setup()
        {
            _ordersProviderMock = new Mock<IOrdersRepository>();
            _ordersProviderMock.Setup(o => o.GetAllOrders(It.IsAny<OrderStatus>()))
                .Returns((OrderStatus status) =>
                    Task.FromResult(OrdersTestData.Where(o => status is null || o.Status == status)));
            _productsProviderMock = new Mock<IProductsRepository>();
            _productsProviderMock.Setup(o => o.GetProductsByMerchantNumbers(It.IsAny<IEnumerable<string>>()))
                .Returns((IEnumerable<string> merchantNums) =>
                {
                    var list =
                        merchantNums
                            .Select(num => ProductsTestData.FirstOrDefault(d => d.MerchantProductNo == num))
                            .Where(res => res != null);
                    return Task.FromResult(list);
                });
        }

        [TestCaseSource(nameof(TestCases))]
        public async Task QueryHandler_ReturnsCorrectFilterResult(ListTopProductsSold.Query query,
            IEnumerable<TopSoldProductDto> expectedDtos)
        {
            var testedHandler =
                new ListTopProductsSold.Handler(_ordersProviderMock.Object, _productsProviderMock.Object);
            var result = await testedHandler.Handle(query, new CancellationToken());
            result.Should().BeEquivalentTo(expectedDtos, options => options.WithStrictOrdering());
        }

        public static IEnumerable TestCases
        {
            get
            {
                var allExpectedDto = CreateDtoList(
                    ("3", 12), ("2", 6), ("5", 5),
                    ("1", 4), ("4", 4), ("6", 4)
                );
                var inProgressDtos =
                    CreateDtoList(("3", 6), ("4", 4), ("5", 4), ("1", 2), ("2", 2));
                yield return new TestCaseData(CreateQuery(6, null), allExpectedDto)
                    .SetName("status: null, quantity:6");
                yield return new TestCaseData(CreateQuery(100, null), allExpectedDto)
                    .SetName("status: null, quantity:100");
                yield return new TestCaseData(CreateQuery(2, null), allExpectedDto.Take(2))
                    .SetName("status: null, quantity:2");
                yield return new TestCaseData(CreateQuery(6, OrderStatus.InProgress), inProgressDtos)
                    .SetName($"status: {OrderStatus.InProgress.Name}, quantity:6");
                yield return new TestCaseData(CreateQuery(3, OrderStatus.InProgress), inProgressDtos.Take(3))
                    .SetName($"status: {OrderStatus.InProgress.Name}, quantity:3");
                yield return new TestCaseData(CreateQuery(0, null), Enumerable.Empty<TopSoldProductDto>())
                    .SetName("status: null, quantity:0");
            }
        }

        private static readonly IEnumerable<Order> OrdersTestData = new[]
        {
            CreateMockOrder(OrderStatus.New, ("1", 1), ("2", 2), ("3", 3)),
            CreateMockOrder(OrderStatus.New, ("1", 1), ("2", 2), ("3", 2)),
            CreateMockOrder(OrderStatus.New, ("3", 1)),
            CreateMockOrder(OrderStatus.InProgress, ("1", 1), ("3", 3), ("4", 4), ("5", 4)),
            CreateMockOrder(OrderStatus.InProgress, ("1", 1), ("2", 2), ("3", 3)),
            CreateMockOrder(OrderStatus.InBackorder, ("6", 1)),
            CreateMockOrder(OrderStatus.InBackorder, ("6", 3), ("5", 1))
        };

        private static readonly IEnumerable<Product> ProductsTestData = new[]
        {
            CreateMockProduct("1"),
            CreateMockProduct("2"),
            CreateMockProduct("3"),
            CreateMockProduct("4"),
            CreateMockProduct("5"),
            CreateMockProduct("6")
        };

        private static ListTopProductsSold.Query CreateQuery(int topQuantity, OrderStatus status)
        {
            return new ListTopProductsSold.Query
            {
                QuantityFilter = topQuantity,
                StatusFilter = status
            };
        }

        private static Product CreateMockProduct(string merchantId)
        {
            return new Product
            {
                MerchantProductNo = merchantId,
                Ean = $"ean{merchantId}",
                Name = $"name{merchantId}"
            };
        }

        private static Order CreateMockOrder(OrderStatus status, params (string merchantId, int quantity)[] lines)
        {
            return new Order
            {
                Status = status,
                Lines = lines.Select(l => new OrderLineItem
                {
                    MerchantProductNo = l.merchantId,
                    Quantity = l.quantity
                }).ToList()
            };
        }

        private static List<TopSoldProductDto> CreateDtoList(params (string merchantId, int quantity)[] items)
        {
            return items.Select(i => new TopSoldProductDto
            {
                Ean = $"ean{i.merchantId}",
                Name = $"name{i.merchantId}",
                MerchantProductNo = i.merchantId,
                QuantitySold = i.quantity
            }).ToList();
        }
    }
}