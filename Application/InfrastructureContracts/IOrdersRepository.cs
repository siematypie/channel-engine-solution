﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Orders;

namespace Application.InfrastructureContracts
{
    public interface IOrdersRepository
    {
        public Task<IEnumerable<Order>> GetAllOrders(OrderStatus statusFilter = null);
    }
}