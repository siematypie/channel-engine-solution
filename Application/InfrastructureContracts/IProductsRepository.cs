﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain;

namespace Application.InfrastructureContracts
{
    public interface IProductsRepository
    {
        Task<IEnumerable<Product>> GetProductsByMerchantNumbers(IEnumerable<string> merchantNumbers);
        Task<Product> GetProductByMerchantNumber(string merchantNumbers);
        Task UpdateProductsStock(string merchantNumber, int newStock);
    }
}