﻿using System;
using System.Net;

namespace Application.Exceptions
{
    public class ChannelEngineAppException : Exception
    {
        public readonly object AdditionalInfo;
        public readonly HttpStatusCode Status;

        public ChannelEngineAppException(HttpStatusCode status, string message, object additionalInfo = null,
            Exception innerException = null)
            : base(message, innerException)
        {
            AdditionalInfo = additionalInfo;
            Status = status;
        }
    }
}