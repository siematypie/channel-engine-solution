﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.InfrastructureContracts;
using Domain.Orders;
using MediatR;

namespace Application.Products
{
    public static class ListTopProductsSold
    {
        public class Query : IRequest<IEnumerable<TopSoldProductDto>>
        {
            public OrderStatus StatusFilter { get; set; }
            public int QuantityFilter { get; set; }
        }

        public class Handler : IRequestHandler<Query, IEnumerable<TopSoldProductDto>>
        {
            private readonly IOrdersRepository _ordersRepository;
            private readonly IProductsRepository _productsRepository;

            public Handler(IOrdersRepository ordersRepository, IProductsRepository productsRepository)
            {
                _ordersRepository = ordersRepository;
                _productsRepository = productsRepository;
            }

            public async Task<IEnumerable<TopSoldProductDto>> Handle(Query request, CancellationToken cancellationToken)
            {
                if (request.QuantityFilter == 0)
                    return Enumerable.Empty<TopSoldProductDto>();

                var allOrders = await _ordersRepository.GetAllOrders(request.StatusFilter);
                var quantityAggregation = allOrders.SelectMany(o => o.Lines)
                    .Aggregate(new Dictionary<string, int>(), AggregateQuantityPerProduct)
                    .OrderByDescending(kv => kv.Value)
                    .ThenBy(kv => kv.Key)
                    .Take(request.QuantityFilter)
                    .ToArray();

                var products = await _productsRepository
                    .GetProductsByMerchantNumbers(quantityAggregation.Select(p => p.Key));

                var result =
                    from aggregatedQuantity in quantityAggregation
                    join product in products.ToList() on aggregatedQuantity.Key equals product.MerchantProductNo
                    select new TopSoldProductDto
                    {
                        Ean = product.Ean,
                        Name = product.Name,
                        QuantitySold = aggregatedQuantity.Value,
                        MerchantProductNo = product.MerchantProductNo
                    };

                return result;
            }

            private Dictionary<string, int> AggregateQuantityPerProduct(Dictionary<string, int> dict,
                OrderLineItem item)
            {
                if (dict.ContainsKey(item.MerchantProductNo))
                    dict[item.MerchantProductNo] += item.Quantity;
                else
                    dict[item.MerchantProductNo] = item.Quantity;
                return dict;
            }
        }
    }
}