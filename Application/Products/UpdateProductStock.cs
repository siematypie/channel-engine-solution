﻿using System.Threading;
using System.Threading.Tasks;
using Application.InfrastructureContracts;
using MediatR;

namespace Application.Products
{
    public static class UpdateProductStock
    {
        public class Command : IRequest
        {
            public string MerchantProductNo { get; set; }
            public int Stock { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly IProductsRepository _productsRepository;

            public Handler(IProductsRepository productsRepository)
            {
                _productsRepository = productsRepository;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                await _productsRepository.UpdateProductsStock(request.MerchantProductNo, request.Stock);
                return Unit.Value;
            }
        }
    }
}