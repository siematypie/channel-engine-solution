﻿using System.Threading;
using System.Threading.Tasks;
using Application.InfrastructureContracts;
using MediatR;

namespace Application.Products
{
    public class GetProductDetails
    {
        public class Query : IRequest<ProductDetailsDto>
        {
            public string MerchantNo { get; set; }
        }

        public class Handler : IRequestHandler<Query, ProductDetailsDto>
        {
            private readonly IProductsRepository _productsRepository;

            public Handler(IProductsRepository productsRepository)
            {
                _productsRepository = productsRepository;
            }

            public async Task<ProductDetailsDto> Handle(Query request, CancellationToken cancellationToken)
            {
                var product = await _productsRepository.GetProductByMerchantNumber(request.MerchantNo);
                return new ProductDetailsDto
                {
                    Name = product.Name,
                    Brand = product.Brand,
                    Color = product.Color,
                    Description = product.Description,
                    Ean = product.Ean,
                    MerchantProductNo = product.MerchantProductNo,
                    Price = product.Price,
                    Size = product.Size,
                    Stock = product.Stock
                };
            }
        }
    }
}