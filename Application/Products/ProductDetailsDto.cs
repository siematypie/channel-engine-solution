﻿namespace Application.Products
{
    public class ProductDetailsDto
    {
        public string Name { get; set; }
        public string MerchantProductNo { get; set; }
        public string Ean { get; set; }
        public string Brand { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public int Stock { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
    }
}