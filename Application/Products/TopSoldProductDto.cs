﻿namespace Application.Products
{
    public class TopSoldProductDto
    {
        public string Name { get; set; }
        public string Ean { get; set; }
        public int QuantitySold { get; set; }
        public string MerchantProductNo { get; set; }
    }
}