﻿using System;
using Domain.Orders;

namespace Application.Orders
{
    public class OrderListItemDto
    {
        public int Id { get; set; }
        public DateTime OrderDate { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public decimal Total { get; set; }
        public string Currency { get; set; }
        public int NumberOfProducts { get; set; }
    }
}