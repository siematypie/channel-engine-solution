﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.InfrastructureContracts;
using Domain.Orders;
using MediatR;

namespace Application.Orders
{
    public static class ListOrders
    {
        public class Query : IRequest<IEnumerable<OrderListItemDto>>
        {
            public OrderStatus StatusFilter { get; set; }
        }

        public class Handler : IRequestHandler<Query, IEnumerable<OrderListItemDto>>
        {
            private readonly IOrdersRepository _ordersRepository;

            public Handler(IOrdersRepository ordersRepository)
            {
                _ordersRepository = ordersRepository;
            }

            public async Task<IEnumerable<OrderListItemDto>> Handle(Query request, CancellationToken cancellationToken)
            {
                var orders = await _ordersRepository.GetAllOrders(request.StatusFilter);
                return orders.Select(o => new OrderListItemDto
                {
                    Id = o.Id,
                    NumberOfProducts = o.Lines.Count,
                    OrderDate = o.CreatedAt,
                    OrderStatus = o.Status,
                    Total = o.TotalInclVat,
                    Currency = o.CurrencyCode
                });
            }
        }
    }
}