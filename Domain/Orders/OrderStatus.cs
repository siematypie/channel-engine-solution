﻿using Ardalis.SmartEnum;

namespace Domain.Orders
{
    public sealed class OrderStatus : SmartEnum<OrderStatus>
    {
        public static readonly OrderStatus InProgress = new OrderStatus("In Progress", "IN_PROGRESS", 1);
        public static readonly OrderStatus Shipped = new OrderStatus("Shipped", "SHIPPED", 2);
        public static readonly OrderStatus InBackorder = new OrderStatus("In Backorder", "IN_BACKORDER", 3);
        public static readonly OrderStatus Manco = new OrderStatus("Manco", "MANCO", 4);
        public static readonly OrderStatus InCombi = new OrderStatus("In Combi", "IN_COMBI", 5);
        public static readonly OrderStatus New = new OrderStatus("New", "NEW", 6);
        public static readonly OrderStatus Returned = new OrderStatus("Returned", "RETURNED", 7);

        public static readonly OrderStatus RequiresCorrection =
            new OrderStatus("Requires Correction", "REQUIRES_CORRECTION", 8);

        public readonly string DisplayName;

        private OrderStatus(string displayName, string name, int value) : base(name, value)
        {
            DisplayName = displayName;
        }
    }
}