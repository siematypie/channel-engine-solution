﻿namespace Domain.Orders
{
    public class OrderLineItem
    {
        public string MerchantProductNo { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPriceInclVat { get; set; }
    }
}