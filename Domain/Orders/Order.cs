﻿using System;
using System.Collections.Generic;

namespace Domain.Orders
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public decimal TotalInclVat { get; set; }
        public string CurrencyCode { get; set; }
        public OrderStatus Status { get; set; }
        public IList<OrderLineItem> Lines { get; set; }
    }
}