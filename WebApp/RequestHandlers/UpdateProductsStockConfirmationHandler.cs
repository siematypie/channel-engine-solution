﻿using System.Threading;
using System.Threading.Tasks;
using Application.Exceptions;
using Application.Products;
using MediatR;
using WebApp.Models;
using WebApp.Utilities;

namespace WebApp.RequestHandlers
{
    public class UpdateProductsStockConfirmationHandler : IPipelineBehavior<UpdateProductStock.Command, Unit>
    {
        private readonly IPageMessageService _messageService;

        public UpdateProductsStockConfirmationHandler(IPageMessageService messageService)
        {
            _messageService = messageService;
        }

        public async Task<Unit> Handle(UpdateProductStock.Command request, CancellationToken cancellationToken,
            RequestHandlerDelegate<Unit> next)
        {
            try
            {
                await next();
                _messageService.AddMessage(MessageType.Success, "Changes saved successfully");
            }
            catch (ChannelEngineAppException)
            {
                _messageService.AddMessage(MessageType.Error, "Something went wrong... Product not updated");
            }

            return Unit.Value;
        }
    }
}