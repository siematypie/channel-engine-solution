﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApp.Models;

namespace WebApp.Utilities
{
    public static class MyHtmlHelpers
    {
        public static IEnumerable<PageMessage> GetPageMessages(this IHtmlHelper htmlHelper)
        {
            return htmlHelper.TempData.Get<List<PageMessage>>(IPageMessageService.MessagesKey) ??
                   Enumerable.Empty<PageMessage>();
        }
    }
}