﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.DependencyInjection;
using WebApp.Models;

namespace WebApp.Utilities
{
    public interface IPageMessageService
    {
        public const string MessagesKey = "PageMessages";
        void AddMessage(MessageType type, string message);
    }

    public class PageMessageService : IPageMessageService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public PageMessageService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void AddMessage(MessageType type, string message)
        {
            var context = _httpContextAccessor?.HttpContext ??
                          throw new InvalidOperationException(
                              "This page message service implementation requires existing HTTP context");
            var pageMessage = new PageMessage {MessageType = type, Value = message};
            var tempDataDictionaryFactory = context.RequestServices.GetRequiredService<ITempDataDictionaryFactory>();
            var tempDataDictionary = tempDataDictionaryFactory.GetTempData(context);
            var existingMessages = tempDataDictionary
                                       .Get<List<PageMessage>>(IPageMessageService.MessagesKey) ??
                                   new List<PageMessage>();
            existingMessages.Add(pageMessage);
            tempDataDictionary.Put(IPageMessageService.MessagesKey, new List<PageMessage> {pageMessage});
        }
    }
}