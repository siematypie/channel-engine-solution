﻿using System.Collections.Generic;
using Application.Orders;
using Domain.Orders;

namespace WebApp.Models
{
    public class OrderListViewModel
    {
        public OrderStatus CurrentFilter { get; set; }
        public IEnumerable<OrderListItemDto> ListItems { get; set; }
    }
}