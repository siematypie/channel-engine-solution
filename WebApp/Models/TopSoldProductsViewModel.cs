﻿using System.Collections.Generic;
using Application.Products;
using Domain.Orders;

namespace WebApp.Models
{
    public class TopSoldProductsViewModel
    {
        public IEnumerable<int> QuantityFilterValues { get; set; }
        public OrderStatus CurrentStatusFilter { get; set; }
        public int CurrentQuantityFilter { get; set; }
        public IEnumerable<TopSoldProductDto> ProductList { get; set; }
    }
}