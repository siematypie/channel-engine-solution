﻿using System.Collections.Generic;
using System.Linq;

namespace WebApp.Models
{
    public class DropdownValues
    {
        public string Url { get; set; }
        public string ActiveValueName { get; set; }

        public IEnumerable<(string name, string value)> AllDropdownEntries { get; set; } =
            new List<(string name, string value)>();

        public IEnumerable<(string key, string value)> OtherQueryParams { get; set; } =
            new List<(string key, string value)>();

        public string FilterParameterName { get; set; }

        public string GetUrl(string dropDownValue)
        {
            var queryString = OtherQueryParams
                .Select(tuple => $"{tuple.key}={tuple.value}").Prepend($"{FilterParameterName}={dropDownValue}");
            return $"{Url}?{string.Join("&", queryString)}";
        }
    }
}