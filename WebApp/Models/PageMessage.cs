﻿namespace WebApp.Models
{
    public class PageMessage
    {
        public string Value { get; set; }
        public MessageType MessageType { get; set; }
    }

    public enum MessageType
    {
        Success,
        Error,
        Info
    }
}