using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using SharedConfiguration;

namespace WebApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((context, configBuilder) =>
                {
                    configBuilder.AddChannelEngineJsonSharedSettings(context.HostingEnvironment);
                    configBuilder.AddJsonFile(
                        $"appsettings.{context.HostingEnvironment.EnvironmentName}.json",
                        true);
                    configBuilder.AddEnvironmentVariables();
                    configBuilder.AddCommandLine(args);

                    if (context.HostingEnvironment.IsDevelopment())
                        configBuilder.AddUserSecrets(typeof(ConfigurationRegistration).Assembly);
                })
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
        }
    }
}