﻿using System;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace WebApp.Controllers
{
    public abstract class BaseController : Controller
    {
        private readonly Lazy<IMediator> _mediator;

        protected BaseController()
        {
            _mediator = new Lazy<IMediator>(() => HttpContext.RequestServices.GetService<IMediator>());
        }

        protected IMediator Mediator => _mediator.Value;
    }
}