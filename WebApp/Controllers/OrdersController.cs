﻿using System.Threading.Tasks;
using Application.Orders;
using Domain.Orders;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class OrdersController : BaseController
    {
        public async Task<IActionResult> List(int statusFilter = 0)
        {
            OrderStatus.TryFromValue(statusFilter, out var filterValue);
            var orders = await Mediator.Send(
                new ListOrders.Query
                {
                    StatusFilter = filterValue
                });
            return View(new OrderListViewModel
            {
                ListItems = orders,
                CurrentFilter = filterValue
            });
        }
    }
}