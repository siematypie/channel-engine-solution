﻿using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class ErrorController : Controller
    {
        private const string Msg404 = "Oops... resource could not be found";
        private const string MsgGeneric = "Oops... somthing went wrong. Please contact application administrator";

        [Route("error/{code:int}")]
        public IActionResult Error(int code)
        {
            var vm = (code, code == (int) HttpStatusCode.NotFound ? Msg404 : MsgGeneric);
            return View(vm);
        }
    }
}