﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Products;
using Domain.Orders;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class ProductsController : BaseController
    {
        private static readonly IReadOnlyList<int> AllowedFilterValues = Array.AsReadOnly(new[] {5, 10, 15});

        public async Task<IActionResult> TopSold(int quantityFilter = 5, int statusFilter = 0)
        {
            OrderStatus.TryFromValue(statusFilter, out var statusFilterValue);
            var quantityFilterValue = AllowedFilterValues.Contains(quantityFilter) ? quantityFilter : 5;
            var products = await Mediator.Send(new ListTopProductsSold.Query
            {
                StatusFilter = statusFilterValue,
                QuantityFilter = quantityFilterValue
            });

            var viewModel = new TopSoldProductsViewModel
            {
                QuantityFilterValues = AllowedFilterValues,
                CurrentStatusFilter = statusFilterValue,
                CurrentQuantityFilter = quantityFilterValue,
                ProductList = products
            };
            return View(viewModel);
        }

        [Route("Products/Edit/{merchantProductNo}")]
        public async Task<IActionResult> Edit(string merchantProductNo)
        {
            var product = await Mediator.Send(new GetProductDetails.Query {MerchantNo = merchantProductNo});
            return View(product);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateStock(string merchantProductNo, int stock)
        {
            await Mediator.Send(new UpdateProductStock.Command
            {
                MerchantProductNo = merchantProductNo,
                Stock = stock
            });
            return RedirectToAction("Edit", new {merchantProductNo});
        }
    }
}